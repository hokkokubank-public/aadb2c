﻿using System;
using System.Configuration;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Interop;
using Microsoft.Owin.Security.Notifications;
using Microsoft.Owin.Security.OpenIdConnect;
using Owin;
using Microsoft.AspNet.Identity;
using AADB2CTrial.Models;
using System.IO;
using Microsoft.AspNetCore.DataProtection.Internal;

[assembly: OwinStartup(typeof(AzureADB2CTest.Startup))]

namespace AzureADB2CTest
{
    public class Startup
    {
        private static string clientId = ConfigurationManager.AppSettings["ida:ClientId"];
        private static string aadInstance = ConfigurationManager.AppSettings["ida:AadInstance"];
        private static string tenant = ConfigurationManager.AppSettings["ida:Tenant"];
        private static string redirectUri = ConfigurationManager.AppSettings["ida:RedirectUri"];
        private static string clientSecret = ConfigurationManager.AppSettings["ida:ClientSecret"];
        public static string SignInPolicyId = ConfigurationManager.AppSettings["ida:SignInPolicyId"];
        public static string ProfilePolicyId = ConfigurationManager.AppSettings["ida:UserProfilePolicyId"];

        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }

        public void ConfigureAuth(IAppBuilder app)
        {
            app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType);

            app.UseCookieAuthentication(new CookieAuthenticationOptions() 
            {
                CookieName = ".AspNet.SharedCookie",
            });
            //{
            //    AuthenticationType = "Identity.Application",
            //    CookieName = ".AspNet.SharedCookie",
            //    LoginPath = new PathString("/Home/Index"),
            //    Provider = new CookieAuthenticationProvider
            //    {
            //        OnValidateIdentity =
            //        SecurityStampValidator
            //            .OnValidateIdentity<ApplicationUserManager, ApplicationUser>(
            //                validateInterval: TimeSpan.FromMinutes(30),
            //                regenerateIdentity: (manager, user) =>
            //                    user.GenerateUserIdentityAsync(manager))
            //    },
            //    TicketDataFormat = new AspNetTicketDataFormat(
            //    new DataProtectorShim(
            //        DataProtectionProvider.Create(new DirectoryInfo("https://aadb2cfilestorage.file.core.windows.net/aadb2ctrial/key-ring/"),
            //            (builder) => { builder.SetApplicationName("AADB2CTrial"); })
            //        .CreateProtector(
            //            "Microsoft.AspNetCore.Authentication.Cookies." +
            //                "CookieAuthenticationMiddleware",
            //            "Identity.Application",
            //            "v2"))),
            //    CookieManager = new ChunkingCookieManager()
            //});
            app.UseOpenIdConnectAuthentication(CreateOptionsFromPolicy(SignInPolicyId));
        }

        private Task AuthenticationFailed(AuthenticationFailedNotification<OpenIdConnectMessage, OpenIdConnectAuthenticationOptions> notification)
        {
            notification.HandleResponse();
            if (notification.Exception.Message == "access_denied")
            {
                notification.Response.Redirect("/");
            }
            else
            {
                notification.Response.Redirect("/Home/Error?message=" + notification.Exception.Message);
            }

            return Task.FromResult(0);
        }

        private OpenIdConnectAuthenticationOptions CreateOptionsFromPolicy(string policy)
        {
            return new OpenIdConnectAuthenticationOptions
            {
                MetadataAddress = String.Format(aadInstance, tenant, policy),
                AuthenticationType = policy,
                ClientId = clientId,
                RedirectUri = redirectUri,
                PostLogoutRedirectUri = redirectUri,
                Notifications = new OpenIdConnectAuthenticationNotifications
                {
                    AuthenticationFailed = AuthenticationFailed
                },
                Scope = "openid",
                ResponseType = "id_token",
                TokenValidationParameters = new TokenValidationParameters
                {
                    NameClaimType = "name",
                    SaveSigninToken = true
                },
                ClientSecret = clientSecret
            };
        }
    }
}